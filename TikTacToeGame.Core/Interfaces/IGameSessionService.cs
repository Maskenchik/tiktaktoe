﻿using TikTacToeGame.Data.Base;
using TikTacToeGame.Data.Models;

namespace TikTacToeGame.Core.Interfaces;

public interface IGameSessionService
{
    Task<Data.Models.TikTacToeGame> CreateGameAsync(bool isFirstBot, CancellationToken cancellationToken = default);
    Task<Data.Models.TikTacToeGame> GetGameByIdAsync(string id, CancellationToken cancellationToken = default);
    Task<Data.Models.TikTacToeGame> MakeMoveAsync(string id, int position, CancellationToken cancellationToken = default);
    Task<PageResponse<Data.Models.TikTacToeGame>> GetGamesAsync(PaginationFilter filter, CancellationToken cancellationToken = default);
    Task<Statistics> GetStatsAsync(CancellationToken cancellationToken = default);
}