﻿using MongoDB.Driver;
using TikTacToeGame.Core.Interfaces;
using TikTacToeGame.Data.Base;
using TikTacToeGame.Data.Enums;
using TikTacToeGame.Data.Models;

namespace TikTacToeGame.Core.Services;

public class GameSessionService : IGameSessionService
{
    private readonly IGameSessionRepository _repository;

    public GameSessionService(IGameSessionRepository repository)
    {
        _repository = repository;
    }

    public async Task<Data.Models.TikTacToeGame> CreateGameAsync(bool isFirstBot, CancellationToken cancellationToken = default)
    {
        var game = new Data.Models.TikTacToeGame(Guid.NewGuid().ToString(), isFirstBot);

        if (isFirstBot)
        {
            await MakeBotMove(game, cancellationToken);
        }

        await _repository.AddGameSessionAsync(game, cancellationToken);

        return game;
    }

    public async Task<Data.Models.TikTacToeGame> GetGameByIdAsync(string id, CancellationToken cancellationToken = default)
    {
        return await _repository.GetByIdAsync(id, cancellationToken);
    }

    public async Task<Data.Models.TikTacToeGame> MakeMoveAsync(string id, int position, CancellationToken cancellationToken = default)
    {
        var game = await GetGameByIdAsync(id, cancellationToken);

        if (game.Board[position] != Symbol.Empty || game.Winner != Symbol.Empty)
        {
            return game;
        }

        game.Board[position] = game.PlayerSymbol;

        if (!game.IsBoardFull(game.Board) && game.IsGameOver() == Symbol.Empty)
        {
            await MakeBotMove(game, cancellationToken); 
        }

        game.Winner = game.IsGameOver();

        await _repository.UpdateGameAsync(id, game, cancellationToken);

        return game;
    }

    public async Task<PageResponse<Data.Models.TikTacToeGame>> GetGamesAsync(PaginationFilter filter, CancellationToken cancellationToken = default)
    {
        return await _repository.GetGamesAsync(filter, cancellationToken);
    }

    public async Task<Statistics> GetStatsAsync(CancellationToken cancellationToken = default)
    {
        var filter = Builders<Data.Models.TikTacToeGame>.Filter.Eq("Winner", Symbol.O) &
                     Builders<Data.Models.TikTacToeGame>.Filter.Eq("PlayerSymbol", Symbol.O)
                     | Builders<Data.Models.TikTacToeGame>.Filter.Eq("Winner", Symbol.X) &
                     Builders<Data.Models.TikTacToeGame>.Filter.Eq("PlayerSymbol", Symbol.X);
        var playerWinner = await _repository.GetCountByFilterAsync(filter, cancellationToken);

        filter = Builders<Data.Models.TikTacToeGame>.Filter.Eq("Winner", Symbol.O) &
                 Builders<Data.Models.TikTacToeGame>.Filter.Eq("BotSymbol", Symbol.O)
                 | Builders<Data.Models.TikTacToeGame>.Filter.Eq("Winner", Symbol.X) &
                 Builders<Data.Models.TikTacToeGame>.Filter.Eq("BotSymbol", Symbol.X);
        var botWinner = await _repository.GetCountByFilterAsync(filter, cancellationToken);

        filter = Builders<Data.Models.TikTacToeGame>.Filter.Eq("Winner", Symbol.Empty);
        var tie = await _repository.GetCountByFilterAsync(filter, cancellationToken);

        return new Statistics()
        {
            BotWinner = botWinner,
            PlayerWinner = playerWinner,
            Tie = tie
        };
    }
    
    private async Task MakeBotMove(Data.Models.TikTacToeGame game, CancellationToken cancellationToken)
    {
        var move = game.GetBestMove(game.Board);
        game.Board[move] = game.BotSymbol;
        
        await _repository.UpdateGameAsync(game.Id, game, cancellationToken);
    }
}