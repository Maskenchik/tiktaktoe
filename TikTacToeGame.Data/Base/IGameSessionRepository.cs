﻿using MongoDB.Driver;
using TikTacToeGame.Data.Models;

namespace TikTacToeGame.Data.Base;

public interface IGameSessionRepository : IRepository<Models.TikTacToeGame>
{
    Task AddGameSessionAsync(Models.TikTacToeGame game, CancellationToken cancellationToken = default);
    Task<Models.TikTacToeGame> GetByIdAsync(string id, CancellationToken cancellationToken = default);
    Task UpdateGameAsync(string id, Models.TikTacToeGame game, CancellationToken cancellationToken = default);
    Task<PageResponse<Models.TikTacToeGame>> GetGamesAsync(PaginationFilter filter, CancellationToken cancellationToken = default);
    Task<long> GetCountByFilterAsync(FilterDefinition<Models.TikTacToeGame> filter, CancellationToken cancellationToken = default);
}