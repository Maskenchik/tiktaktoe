﻿using MongoDB.Driver;

namespace TikTacToeGame.Data.Base;

public interface IRepository<T>
{
    IMongoCollection<T> Collection { get; }
}