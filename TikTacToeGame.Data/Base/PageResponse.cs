﻿namespace TikTacToeGame.Data.Base;

public class PageResponse<T>
{
    public IEnumerable<T> Result { get; set; }

    public long TotalRows { get; set; } = 1;
}