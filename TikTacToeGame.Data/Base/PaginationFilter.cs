﻿namespace TikTacToeGame.Data.Base;

public class PaginationFilter
{
    public int PageSize { get; set; } = 10;
    public int Page { get; set; } = 1;
}