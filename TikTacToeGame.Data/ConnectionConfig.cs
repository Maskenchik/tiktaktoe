﻿namespace TikTacToeGame.Data;

public class ConnectionConfig
{
    public string ConnectionString { get; set; }
    public string DatabaseName { get; set; }
}