﻿namespace TikTacToeGame.Data.Enums;

public enum Symbol
{
    O = 0,
    X = 1,
    Empty = 2
}