﻿namespace TikTacToeGame.Data.Models;

public class Statistics
{
    public long BotWinner { get; set; }
    public long PlayerWinner { get; set; }
    public long Tie { get; set; }
}