﻿using TikTacToeGame.Data.Enums;

namespace TikTacToeGame.Data.Models;

public class TikTacToeGame
{
    public TikTacToeGame(string id, bool isFirstBot)
    {
        BotSymbol = isFirstBot ? Symbol.X : Symbol.O;
        PlayerSymbol = isFirstBot ? Symbol.O : Symbol.X;
        Winner = Symbol.Empty;
        Id = id;
        Board = new Symbol[9]
        {
            Symbol.Empty, Symbol.Empty, Symbol.Empty, Symbol.Empty, Symbol.Empty, Symbol.Empty, Symbol.Empty,
            Symbol.Empty, Symbol.Empty
        };
    }
    public string Id { get; set; }
    public Symbol[] Board { get; set; }
    public Symbol PlayerSymbol { get; set; }
    public Symbol BotSymbol { get; set; }
    public Symbol Winner { get; set; }

    public int GetBestMove(Symbol[] board)
    {
        int bestScore = int.MinValue;
        int bestMove = -1;

        for (int i = 0; i < 9; i++)
        {
            if (IsMoveValid(board, i))
            {
                board[i] = BotSymbol;
                int score = MiniMax(board, 0, false);
                board[i] = Symbol.Empty;

                if (score > bestScore)
                {
                    bestScore = score;
                    bestMove = i;
                }
            }
        }

        return bestMove;
    }

    public Symbol IsGameOver()
    {
        int[][] winningPositions = new int[][]
        {
            new int[] { 0, 1, 2 }, new int[] { 3, 4, 5 }, new int[] { 6, 7, 8 }, // горизонтальные линии
            new int[] { 0, 3, 6 }, new int[] { 1, 4, 7 }, new int[] { 2, 5, 8 }, // вертикальные линии
            new int[] { 0, 4, 8 }, new int[] { 2, 4, 6 } // диагональные линии
        };

        foreach (var positions in winningPositions)
        {
            Symbol firstSymbol = Board[positions[0]];
            if (firstSymbol != Symbol.Empty && Board[positions[1]] == firstSymbol && Board[positions[2]] == firstSymbol)
            {
                return firstSymbol;
            }
        }

        return Symbol.Empty; // возвращаем ' ', если победитель не найден
    }

    public bool IsBoardFull(Symbol[] board)
    {
        foreach (Symbol cell in board)
        {
            if (cell == Symbol.Empty)
            {
                return false;
            }
        }

        return true;
    }
    
    private int MiniMax(Symbol[] board, int depth, bool isMaximizingPlayer)
    {
        if (IsWinningMove(board, BotSymbol))
        {
            return 1;
        }
        else if (IsWinningMove(board, PlayerSymbol))
        {
            return -1;
        }
        else if (IsBoardFull(board))
        {
            return 0;
        }

        if (isMaximizingPlayer)
        {
            int bestScore = int.MinValue;

            for (int i = 0; i < 9; i++)
            {
                if (IsMoveValid(board, i))
                {
                    board[i] = BotSymbol;
                    int score = MiniMax(board, depth + 1, false);
                    board[i] = Symbol.Empty;

                    bestScore = Math.Max(score, bestScore);
                }
            }

            return bestScore;
        }
        else
        {
            int bestScore = int.MaxValue;

            for (int i = 0; i < 9; i++)
            {
                if (IsMoveValid(board, i))
                {
                    board[i] = PlayerSymbol;
                    int score = MiniMax(board, depth + 1, true);
                    board[i] = Symbol.Empty;

                    bestScore = Math.Min(score, bestScore);
                }
            }

            return bestScore;
        }
    }

    private bool IsMoveValid(Symbol[] board, int move)
    {
        return board[move] == Symbol.Empty;
    }

    private bool IsWinningMove(Symbol[] board, Symbol symbol)
    {
        if ((board[0] == symbol && board[1] == symbol && board[2] == symbol) || // Верхняя горизонталь
            (board[3] == symbol && board[4] == symbol && board[5] == symbol) || // Средняя горизонталь
            (board[6] == symbol && board[7] == symbol && board[8] == symbol) || // Нижняя горизонталь
            (board[0] == symbol && board[3] == symbol && board[6] == symbol) || // Левая вертикаль
            (board[1] == symbol && board[4] == symbol && board[7] == symbol) || // Средняя вертикаль
            (board[2] == symbol && board[5] == symbol && board[8] == symbol) || // Правая вертикаль
            (board[0] == symbol && board[4] == symbol && board[8] == symbol) || // Главная диагональ
            (board[2] == symbol && board[4] == symbol && board[6] == symbol))   // Побочная диагональ
        {
            return true;
        }

        return false;
    }
}