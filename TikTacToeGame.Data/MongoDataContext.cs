﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace TikTacToeGame.Data;

public class MongoDataContext
{
    private readonly IMongoDatabase _database;

    public MongoDataContext(IOptions<ConnectionConfig> options)
    {
        var client = new MongoClient(options.Value.ConnectionString);
        _database = client.GetDatabase(options.Value.DatabaseName);
    }

    public IMongoCollection<T> GetCollection<T>(string collectionName)
    {
        return _database.GetCollection<T>(collectionName);
    }
}