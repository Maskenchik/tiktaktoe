﻿using MongoDB.Driver;
using TikTacToeGame.Data.Base;

namespace TikTacToeGame.Data.Repositories;

public class GameSessionRepository : RepositoryBase<Models.TikTacToeGame>, IGameSessionRepository
{
    public IMongoCollection<Models.TikTacToeGame> Collection { get; }

    public GameSessionRepository(MongoDataContext dataContext) : base(dataContext, "gameSessions")
    {
    }

    public async Task AddGameSessionAsync(Models.TikTacToeGame game, CancellationToken cancellationToken = default)
    {
        await _collection.InsertOneAsync(game, cancellationToken: cancellationToken);
    }

    public async Task<Models.TikTacToeGame> GetByIdAsync(string id, CancellationToken cancellationToken = default)
    {
        var games = await _collection.FindAsync(g => g.Id == id, cancellationToken: cancellationToken);
        return games.FirstOrDefault();
    }

    public async Task UpdateGameAsync(string id, Models.TikTacToeGame game, CancellationToken cancellationToken = default)
    {
        await _collection.ReplaceOneAsync(g => g.Id == id, game, cancellationToken: cancellationToken);
    }

    public async Task<PageResponse<Models.TikTacToeGame>> GetGamesAsync(PaginationFilter filter, CancellationToken cancellationToken = default)
    {
        var tikTacToeGames = _collection.Find(_ => true)
            .Skip((filter.Page - 1) * filter.PageSize)
            .Limit(filter.PageSize)
            .ToList();
        var count = await _collection.CountDocumentsAsync(_ => true, cancellationToken: cancellationToken);
        return new PageResponse<Models.TikTacToeGame>()
        {
            TotalRows = count,
            Result = tikTacToeGames
        };
    }

    public async Task<long> GetCountByFilterAsync(FilterDefinition<Models.TikTacToeGame> filter, CancellationToken cancellationToken = default)
    {
        return await _collection.CountDocumentsAsync(filter, cancellationToken: cancellationToken);
    }
}