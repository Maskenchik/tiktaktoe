﻿using MongoDB.Driver;
using TikTacToeGame.Data.Base;

namespace TikTacToeGame.Data.Repositories;

public abstract class RepositoryBase<T> : IRepository<T>
{
    protected readonly IMongoCollection<T> _collection;

    public RepositoryBase(MongoDataContext dataContext, string collectionName)
    {
        _collection = dataContext.GetCollection<T>(collectionName);
    }

    public IMongoCollection<T> Collection => _collection;
}