using Microsoft.AspNetCore.Mvc;
using TikTacToeGame.Core.Interfaces;
using TikTacToeGame.Data.Base;
using TikTacToeGame.Data.Models;

namespace TikTacToeGame.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TicTacToeController : ControllerBase
{
    private readonly IGameSessionService _game;

    public TicTacToeController(IGameSessionService game)
    {
        _game = game;
    }

    [HttpPost("create")]
    public async Task<Data.Models.TikTacToeGame> CreateGame(bool isFirstPlayerBot = false, CancellationToken cancellationToken = default)
    {
        return await _game.CreateGameAsync(isFirstPlayerBot, cancellationToken);
    }

    [HttpGet("{id}")]
    public async Task<Data.Models.TikTacToeGame> GetGame(string id, CancellationToken cancellationToken = default)
    {
        return await _game.GetGameByIdAsync(id, cancellationToken);
    }

    [HttpPost("move")]
    public async Task<Data.Models.TikTacToeGame> MakeMove(string id, int position, CancellationToken cancellationToken = default)
    {
        return await _game.MakeMoveAsync(id, position, cancellationToken);
    }
    
    [HttpPost("games")]
    public async Task<PageResponse<Data.Models.TikTacToeGame>> GetGames([FromQuery] PaginationFilter filter, CancellationToken cancellationToken = default)
    {
        return await _game.GetGamesAsync(filter, cancellationToken);
    }
    
    [HttpPost("stats")]
    public async Task<Statistics> GetStats(CancellationToken cancellationToken = default)
    {
        return await _game.GetStatsAsync(cancellationToken);
    }
}