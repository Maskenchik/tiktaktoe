﻿using Microsoft.Extensions.Options;
using TikTacToeGame.Core.Interfaces;
using TikTacToeGame.Core.Services;
using TikTacToeGame.Data;
using TikTacToeGame.Data.Base;
using TikTacToeGame.Data.Repositories;

namespace TikTacToeGame;

public static class MainConfiguration
{
    public static void AddMongoDb(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .Configure<ConnectionConfig>(configuration.GetSection("MongoConnection"))
            .AddSingleton<MongoDataContext>(sp =>
            {
                var options = sp.GetRequiredService<IOptions<ConnectionConfig>>();
                return new MongoDataContext(options);
            });
    }

    public static void AddServices(this IServiceCollection services)
    {
        services.AddScoped<IGameSessionRepository, GameSessionRepository>();
        services.AddScoped<IGameSessionService, GameSessionService>();
    }
}